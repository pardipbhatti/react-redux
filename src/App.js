import React, { Component } from 'react';
import { connect } from 'react-redux';
import logo from './logo.svg';
import './App.css';

import AddTodo from './components/AddTodo/AddTodo';
import List from './components/List/List';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to {this.props.userName}</h1>
        </header>
          <AddTodo/>
          <List/>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
    userName: state.user.userName
})

export default connect(mapStateToProps)(App);
