import { combineReducers } from 'redux';
import todos from './todos';
import user from './user';
import about from './about';

const rootReducer = combineReducers({
    todos,
    user,
    about
});

export default rootReducer;