import React from 'react';
import { connect } from 'react-redux';

const About = (props) => (
    <div>
        <h1>{props.aboutTitle}</h1>
        <p>{props.aboutPage}</p>
    </div>
);

const mapStateToProps = (state) => ({
    aboutTitle: state.about.title,
    aboutPage: state.about.aboutMe
})

export default connect(mapStateToProps)(About);