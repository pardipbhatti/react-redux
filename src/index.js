import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import store from './store';

import Header from './Header/Header';
import About from './components/About/About';

const checkAuth = () => {
    return true;
}

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        checkAuth() ? (
            <Component {...props}/>
        ) : (
            <Redirect to={{pathname: '/login'}}/>
        )
    )}/>
)

const ContactUs = () => (
    <div>
        <h1>Contact Us Here</h1>
    </div>
)

const Login = () => (
    <div>
        <h1>You have to login first</h1>
    </div>
)

const AppRouter = () => {
    return(
        <BrowserRouter>
            <div>
                <Header />
                <Route path="/" component={App} exact={true}/>
                <Route path="/login" component={Login}/>
                <PrivateRoute path="/about" component={About}/>
                <PrivateRoute path="/contact" component={ContactUs}/>
            </div>
        </BrowserRouter>
    )
}


ReactDOM.render(
    <Provider store={store}>
        <AppRouter />
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
